#!/bin/sh
# from https://man.archlinux.org/man/i3lock.1#DPMS
revert() {
  xset dpms 0 0 0
}
trap revert HUP INT TERM
xset +dpms dpms 5 5 5
i3lock -n -c "#282a36"
revert
